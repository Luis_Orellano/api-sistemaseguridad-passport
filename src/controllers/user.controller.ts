//definir metodos para utenticar y logear user
import { Request, Response } from "express";
import User, { IUser } from "../models/user";
import jwt from "jsonwebtoken";
import config from "../config/config";

function createToken(user: IUser) {
  return jwt.sign({ id: user.id, name: user.name }, config.JwtSecret, {
    expiresIn: 86400, //= a 1 dia
  });
}

export const singUp = async (
  req: Request,
  res: Response
): Promise<Response> => {
  if (!req.body.name || !req.body.password) {
    return res
      .status(400)
      .json({ msg: "Por favor. Escriba su nombre y password" });
  }
  const user = await User.findOne({ name: req.body.name });
  console.log(user);

  if (user) {
    return res.status(400).json({ msg: "El usuario ya existe" });
  }

  const newUser = new User(req.body);
  await newUser.save();

  return res.status(201).json(newUser);
};

export const singIn = async (req: Request, res: Response) => {
  if (!req.body.name || !req.body.password) {
    return res
      .status(400)
      .json({ msg: "Por favor. Escriba su nombre y password" });
  }

  const user = await User.findOne({ name: req.body.name });
  if (!user) {
    return res.status(400).json({ msg: "El usuario no existe" });
  }

  const isMatch = await user.comparePassword(req.body.password);
  if (isMatch) {
    return res.status(200).json({ token: createToken(user) });
  }

  return res.status(400).json({
    msg: "El nombre o password son incorrectos",
  });
};
