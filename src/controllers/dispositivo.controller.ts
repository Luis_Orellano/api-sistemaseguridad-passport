import { Request, Response } from "express";
import Dispositivo from "../models/dispositivos";

export const addDispositivo = async (req: Request, res: Response) => {
  if (
    !req.body.name ||
    !req.body.port ||
    !req.body.temperature ||
    !req.body.state ||
    !req.body.temperatureMax ||
    !req.body.servicios
  ) {
    return res.status(400).json({
      msg: "Por favor. Escriba los datos del dispositivo correctamente",
    });
  }

  const dispositive = await Dispositivo.findOne({ name: req.body.name });
  console.log(dispositive);

  if (dispositive) {
    return res.status(400).json({ msg: "El dispositivo ya existe" });
  }

  const newDispositivo = new Dispositivo(req.body);
  await newDispositivo.save();

  return res.status(201).json(newDispositivo);
};

export const getDispositives = async (req: Request, res: Response) => {
  const dispositives = await Dispositivo.find();
  return res.status(201).json(dispositives);
};
