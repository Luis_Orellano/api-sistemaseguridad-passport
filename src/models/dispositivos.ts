import { model, Schema, Document } from "mongoose";
import { IServicio, servicioSchema } from "./servicio";

export interface IDispositivo extends Document {
  name: String;
  temperature: number;
  port: number;
  state: boolean;
  temperatureMax: number;
  servicios: IServicio[];
}

const dispositivoSchema = new Schema<IDispositivo>({
  name: {
    type: String,
    require: true,
  },
  temperature: {
    type: Number,
    required: true,
  },
  port: {
    type: Number,
    required: true,
  },
  state: {
    type: Boolean,
    require: true,
  },
  temperatureMax: {
    type: Number,
    require: true,
  },
  servicios: [servicioSchema],
});

export default model<IDispositivo>("Dispositivo", dispositivoSchema);
