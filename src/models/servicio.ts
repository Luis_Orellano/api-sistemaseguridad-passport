import { model, Schema, Document } from "mongoose";

export interface IServicio extends Document {
  name: string;
  telefono: number;
}

export const servicioSchema = new Schema<IServicio>({
  name: {
    type: String,
    required: true,
  },
  telefono: {
    type: Number,
    required: true,
  },
});

export default model<IServicio>("Servicio", servicioSchema);
