import express from "express";
import morgan from "morgan";
import cors from "cors";
import passport from "passport";
import passportMiddleware from "./middlewares/passport";

import authRoutes from "./routes/auth.routes";
import specialRoutes from "./routes/special.routes";

//initializations
const app = express();

//settings
app.set("port", process.env.PORT || 3000);

//middlewares
app.use(morgan("dev")); //modulo de desarrllo de morgan
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json()); //para que el servidor entienda los json
app.use(passport.initialize());
passport.use(passportMiddleware); //configuracion del passport

//routes
app.get("/", (req, res) => {
  res.send(`La API se inicio en http://localhost:${app.get("port")}`);
});

app.use(authRoutes);
app.use(specialRoutes);

export default app;
