import mongoose, { ConnectOptions } from "mongoose"; //modulo de coneccion a la base de dato mongodb

import config from "./config/config";

const dbOptions: ConnectOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
};

mongoose.connect(config.DB.URI, dbOptions);

const connection = mongoose.connection;

connection.once("open", () => {
  console.log("MongoDB coneccion establecida");
});

connection.on("error", (err) => {
  console.log(err);
  process.exit(0);
});
