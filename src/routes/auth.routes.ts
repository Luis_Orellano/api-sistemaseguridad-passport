import { Router } from "express";
const router = Router();

import { singIn, singUp } from "../controllers/user.controller";
import {
  addDispositivo,
  getDispositives,
} from "../controllers/dispositivo.controller";

router.post("/signup", singUp);
router.post("/signin", singIn);

router.post("/dispositives", addDispositivo);
router.get("/dispositives", getDispositives);

export default router;
